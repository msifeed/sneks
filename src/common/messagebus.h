#ifndef MESSAGE_BUS_HPP
#define MESSAGE_BUS_HPP

#include <list>
#include <memory>
#include <mutex>

#include "message.h"

class MessageBus {
 private:
  std::list<std::unique_ptr<Message>> bus_;
  std::mutex mutex_;

 public:
  MessageBus() {}

  bool is_empty() {
    std::lock_guard<std::mutex> lck(mutex_);
    return bus_.empty();
  }
  void put(std::unique_ptr<Message> msg) {
    std::lock_guard<std::mutex> lck(mutex_);
    bus_.push_back(std::move(msg));
  }
  void put_all(std::list<std::unique_ptr<Message>>& msg_list) {
    std::lock_guard<std::mutex> lck(mutex_);
    bus_.splice(bus_.end(), msg_list);
  }
  std::unique_ptr<Message> pop() {
    std::lock_guard<std::mutex> lck(mutex_);
    std::unique_ptr<Message> tmp = std::move(bus_.front());
    bus_.pop_front();
    return tmp;
  }
  std::list<std::unique_ptr<Message>> consume_messages() {
    std::lock_guard<std::mutex> lck(mutex_);
    std::list<std::unique_ptr<Message>> tmp;
    tmp.swap(bus_);
    return tmp;
  }
  void remove_with_sid(uint32_t sid) {
    std::lock_guard<std::mutex> lck(mutex_);
    bus_.remove_if(
        [sid](const std::unique_ptr<Message>& m) { return m->sid == sid; });
  }
};

#endif  // MESSAGE_BUS_HPP