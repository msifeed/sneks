#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <cinttypes>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <string>
#include <vector>

#include "snake.h"

enum MessageType { SPAWN, TURN, OBJECTS };

struct Message {
  uint64_t id = 0;
  uint32_t sid = 0;

  virtual ~Message() {}
  virtual MessageType type() = 0;
  template <class Archive>
  void serialize(Archive& ar, const unsigned int) {
    ar& id;
  }
};

struct MessageSpawn : public Message {
  std::string name;

  MessageSpawn() {}
  virtual MessageType type() { return MessageType::SPAWN; }

  template <class Archive>
  void serialize(Archive& ar, const unsigned int) {
    boost::serialization::void_cast_register<MessageSpawn, Message>();
    ar& boost::serialization::base_object<Message>(*this);
    ar& name;
  }
};

struct MessageTurn : public Message {
  float angle;

  MessageTurn() {}
  virtual MessageType type() { return MessageType::TURN; }

  template <class Archive>
  void serialize(Archive& ar, const unsigned int) {
    boost::serialization::void_cast_register<MessageTurn, Message>();
    ar& boost::serialization::base_object<Message>(*this);
    ar& angle;
  }
};

struct MessageObjects : public Message {
  std::vector<Food> foods;
  std::vector<Scale> scales;
  glm::vec3 player_head;

  MessageObjects() {}
  virtual MessageType type() { return MessageType::OBJECTS; }

  template <class Archive>
  void serialize(Archive& ar, const unsigned int) {
    boost::serialization::void_cast_register<MessageObjects, Message>();
    ar& boost::serialization::base_object<Message>(*this);
    ar& foods;
    ar& scales;
    ar& player_head;
  }
};

namespace boost {
namespace serialization {

template <class Archive>
void serialize(Archive& ar, Food& f, const unsigned int) {
  ar& f.pos;
}

template <class Archive>
void serialize(Archive& ar, Scale& s, const unsigned int) {
  ar& s.pos;
  ar& s.color;
}

template <class Archive>
void serialize(Archive& ar, glm::vec3& v, const unsigned int) {
  ar& v.x;
  ar& v.y;
  ar& v.z;
}

}  // namespace serialization
}

#endif  // MESSAGE_HPP