#ifndef SNAKE_H
#define SNAKE_H

#include <cinttypes>
#include <glm/vec3.hpp>
#include <list>
#include <memory>

const float SCALES_PERIOD = 0.5f;

struct Food {
  glm::vec3 pos;
};

struct Scale {
  glm::vec3 pos;
  glm::vec3 color;
};

class Snake {
 private:
 public:
  std::string name_;
  float direction_;
  float next_direction_;
  glm::vec3 color_;
  std::list<Scale> scales_;

  Snake(std::string name) : name_(name) {}
  Scale head() { return scales_.front(); }
  uint32_t len() { return scales_.size(); }
};

typedef std::shared_ptr<Snake> Snake_ptr;

#endif  // SNAKE_H