#ifndef SESSION_HPP
#define SESSION_HPP

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/asio.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <functional>

#include "message.h"

using boost::asio::ip::tcp;
using boost::asio::streambuf;

class Session;

typedef std::shared_ptr<Session> Session_ptr;
typedef std::unique_ptr<Message> Message_ptr;
typedef std::function<void(const boost::system::error_code&, Session&)>
    SessionCallback;

class Session : public std::enable_shared_from_this<Session> {
 private:
  enum { header_length = 4 };

  uint32_t id_ = 0;
  tcp::socket socket_;

  uint8_t inbound_header_[header_length];
  streambuf inbound_data_;

  uint8_t outbound_header_[header_length];
  streambuf outbound_data_;

  uint64_t message_counter_ = 0;
  std::list<Message_ptr> read_queue_;
  std::list<Message_ptr> write_queue_;

 public:
  Session(boost::asio::io_service& io_service) : socket_(io_service) {}

  int id() { return id_; }
  void set_id(int id) { id_ = id; }

  tcp::socket& socket() { return socket_; }

  std::list<Message_ptr> consume_messages() {
    std::list<Message_ptr> ls;
    read_queue_.swap(ls);
    return ls;
  }

  std::list<Message_ptr>& read_queue() { return read_queue_; }

  void queue_message(Message_ptr msg) {
    message_counter_ += 1;
    msg->id = message_counter_;
    write_queue_.push_back(std::move(msg));
  }

  void async_write(SessionCallback callback) {
    std::vector<boost::asio::const_buffer> buffers;

    memset(&outbound_header_[0], 0, sizeof(outbound_header_));
    outbound_data_.consume(outbound_data_.size());

    // Serialize the data first
    std::ostream archive_stream(&outbound_data_);
    boost::archive::binary_oarchive archive(archive_stream);
    archive& write_queue_;

    uint32_t length = static_cast<uint32_t>(outbound_data_.size());
    outbound_header_[0] = (length >> 24) & 0xFF;
    outbound_header_[1] = (length >> 16) & 0xFF;
    outbound_header_[2] = (length >> 8) & 0xFF;
    outbound_header_[3] = (length & 0xFF);

    buffers.push_back(boost::asio::buffer(outbound_header_));
    buffers.push_back(boost::asio::buffer(outbound_data_.data()));

    boost::asio::async_write(
        socket_, buffers,
        [this, callback](const boost::system::error_code& ec, std::size_t) {
          callback(ec, *this);
        });
    write_queue_.clear();
  }

  void async_read(SessionCallback callback) {
    // Clean buffers
    memset(&inbound_header_[0], 0, sizeof(inbound_header_));
    inbound_data_.consume(inbound_data_.size());

    // Read header
    boost::asio::async_read(socket_, boost::asio::buffer(inbound_header_),
                            std::bind(&Session::handle_read_header, this,
                                      std::placeholders::_1, callback));
  }

  void handle_read_header(const boost::system::error_code& ec,
                          SessionCallback callback) {
    if (ec) {
      callback(ec, *this);
      return;
    }

    uint32_t length = (inbound_header_[0] << 24) | (inbound_header_[1] << 16) |
                      (inbound_header_[2] << 8) | (inbound_header_[3]);
    if (length > 10000) {
      std::cerr << "=read len abort" << std::endl;
      boost::system::error_code error(boost::asio::error::message_size);
      callback(error, *this);
      return;
    }

    inbound_data_.prepare(length);

    // Read data
    boost::asio::async_read(socket_, inbound_data_,
                            boost::asio::transfer_exactly(length),
                            std::bind(&Session::handle_read_data, this,
                                      std::placeholders::_1, callback));
  }

  void handle_read_data(const boost::system::error_code& ec,
                        SessionCallback callback) {
    if (ec) {
      callback(ec, *this);
      return;
    }

    try {
      std::istream archive_stream(&inbound_data_);
      boost::archive::binary_iarchive archive(archive_stream);

      std::list<Message_ptr> messages;
      archive >> messages;

      for (Message_ptr& msg : messages) {
        msg->sid = id_;
      }

      read_queue_.splice(read_queue_.end(), messages);
    } catch (std::exception& e) {
      std::cerr << "=read decode error=" << std::endl;
      // Unable to decode data.
      boost::system::error_code error(boost::asio::error::invalid_argument);
      callback(error, *this);
      return;
    }

    // Inform caller that data has been received ok.
    callback(ec, *this);
    async_read(callback);
  }
};

#endif  // SESSION_HPP