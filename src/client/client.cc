#include "client.h"

void Client::run() {
  while (!render_.window_should_close()) {
    glfwPollEvents();

    check_messages();
    render_.draw();

    if (stage_ == GameStage::GAME) send_head_turn();
  }

  network_.stop();
  network_thread_.detach();
}

void Client::try_connect(char* addr, char* port) {
  if (is_connecting_) return;

  std::cout << "try connect" << std::endl;
  is_connecting_ = true;
  network_thread_ = std::thread(&NetworkClient::connect, &network_, addr, port);
}

void Client::try_spawn(std::string name) {
  if (stage_ != GameStage::LOBBY) return;

  auto msg = new MessageSpawn();
  msg->name = name;

  network_.send(std::unique_ptr<Message>(msg));
}

void Client::handle_connect(bool connected) {
  if (connected) {
    stage(GameStage::LOBBY);
  } else {
    is_connecting_ = false;
    network_.stop();
    network_thread_.detach();

    std::cout << "stop connect" << std::endl;
  }
}

void Client::check_messages() {
  for (auto& msg : bus_.consume_messages()) {
    MessageType type = msg->type();
    switch (type) {
      case MessageType::SPAWN:
        // if (static_cast<MessageSpawn&>(*msg).name == "spawn") {
        if (stage_ == GameStage::LOBBY) {
          stage(GameStage::GAME);
        } else {
          stage(GameStage::LOBBY);
        }
        break;
      case MessageType::OBJECTS:
        handle_objects(static_cast<MessageObjects&>(*msg));
        break;
      default:
        break;
    }
  }
}

void Client::handle_objects(MessageObjects& msg) {
  // std::cout << "Client got " << msg.foods.size() << " foods" << std::endl;
  // std::cout << "Client got scales: " << msg.scales.size() << std::endl;
  // for (glm::vec3 scale : msg.scales) {
  //   std::cout << " x:" << scale.x << " y:" << scale.y << " z:" << scale.z
  //             << std::endl;
  // }

  render_.update_field(msg.foods, msg.scales);
  render_.update_player_head(msg.player_head);
}

void Client::send_head_turn() {
  using namespace std::chrono;
  auto now = system_clock::now();
  auto passed = now - last_turn_send_;

  if (passed < milliseconds(80)) {
    return;
  }

  glm::dvec2 cur;
  glfwGetCursorPos(render_.window(), &cur.x, &cur.y);
  cur -= glm::vec2(640 / 2, 480 / 2);  // TODO dynamic

  auto msg = new MessageTurn();
  msg->angle = glm::atan(cur.x, cur.y);

  network_.send(std::unique_ptr<Message>(msg));
}