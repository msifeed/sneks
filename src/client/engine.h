#ifndef ENGINE_H
#define ENGINE_H

enum GameStage {
  SELECT_SERVER,
  SELECT_NAME,
  GAME
}

class Engine {
 private:
  GameStage stage_;

 public:
  void init();
};

#endif  // ENGINE_H