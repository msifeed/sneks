#include "client.h"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Message)
BOOST_CLASS_EXPORT(MessageSpawn)
BOOST_CLASS_EXPORT(MessageTurn)
BOOST_CLASS_EXPORT(MessageObjects)

int main(int, char**) {
  Client& client = Client::instance();
  client.run();

  return 0;
}
