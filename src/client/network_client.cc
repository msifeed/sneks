#include "network_client.h"

void NetworkClient::connect(const std::string& host,
                            const std::string& service) {
  tcp::resolver resolver(io_service_);
  tcp::resolver::iterator endpoint_iterator = resolver.resolve({host, service});

  boost::asio::async_connect(
      session_.socket(), endpoint_iterator,
      std::bind(&NetworkClient::handle_connect, this, std::placeholders::_1));

  // deadline_.expires_from_now(boost::posix_time::seconds(10));
  // deadline_.async_wait(std::bind(&NetworkClient::check_deadline, this));

  io_service_.run();
}

void NetworkClient::stop() {
  io_service_.stop();
  session_.socket().close();
  // deadline_.cancel();
  // io_service_.reset();
}

void NetworkClient::send(std::unique_ptr<Message> msg) {
  session_.queue_message(std::move(msg));
  session_.async_write(write_handler_);
}

void NetworkClient::check_deadline() {
  if (deadline_.expires_at() <=
      boost::asio::deadline_timer::traits_type::now()) {
    session_.socket().close();
    deadline_.expires_at(boost::posix_time::pos_infin);
  }

  deadline_.async_wait(std::bind(&NetworkClient::check_deadline, this));
}

void NetworkClient::handle_connect(const boost::system::error_code& ec) {
  if (ec) {
    std::cerr << "Client connect error: " << ec.message() << std::endl;
    connect_cb_(false);
  } else {
    std::cout << "Client connected" << std::endl;
    connect_cb_(true);
    session_.async_read(std::bind(&NetworkClient::handle_read, this,
                                  std::placeholders::_1,
                                  std::placeholders::_2));
  }
}

void NetworkClient::handle_read(const boost::system::error_code& ec,
                                Session& sess) {
  if (ec) {
    if (ec == boost::asio::error::eof ||
        ec == boost::asio::error::connection_reset) {
      // TODO handle me
      std::cerr << "Client disconnect" << std::endl;
    } else {
      std::cerr << "Client read error: " << ec.message() << std::endl;
    }
  } else {
    bus_.put_all(sess.read_queue());
  }
}

void NetworkClient::handle_write(const boost::system::error_code& ec,
                                 Session&) {
  if (ec) {
    std::cerr << "Client write error: " << ec.message() << std::endl;
  }
}