#ifndef CLIENT_H
#define CLIENT_H

#include <chrono>
#include <functional>
#include <thread>

#include "common/messagebus.h"
#include "network_client.h"
#include "render.h"

enum GameStage { MAIN_MENU, LOBBY, GAME };

class Client {
 private:
  MessageBus bus_;
  NetworkClient network_;
  std::thread network_thread_;

  Render render_;

  bool is_running_ = true;
  bool is_connecting_ = false;
  GameStage stage_ = GameStage::MAIN_MENU;

  std::chrono::system_clock::time_point last_turn_send_;

 public:
  static Client& instance() {
    static Client inst;
    return inst;
  }

  Client(Client const&) = delete;
  void operator=(Client const&) = delete;

  void run();
  GameStage stage() { return stage_; }
  void stage(GameStage st) { stage_ = st; }

  void try_connect(char* addr, char* port);
  void try_spawn(std::string name);

 private:
  Client()
      : network_(bus_, std::bind(&Client::handle_connect, this,
                                 std::placeholders::_1)) {
    render_.init();
  }

  void check_messages();

  void send_head_turn();
  void handle_connect(bool);
  void handle_objects(MessageObjects& msg);
};

#endif  // CLIENT_H