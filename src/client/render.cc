#include "render.h"
#include "render_utils.h"

#include "client.h"

Render::Render() { glfwInit(); }

Render::~Render() {
  glDeleteBuffers(1, &particles_vertex_buffer_);
  glDeleteBuffers(1, &scales_data_buffer_);
  glDeleteBuffers(1, &food_data_buffer_);

  glDeleteProgram(food_shader_prog_);
  glDeleteProgram(scales_shader_prog_);
  glDeleteVertexArrays(1, &vertex_array_id_);

  ImGui_ImplGlfwGL3_Shutdown();
  glfwDestroyWindow(window_);
  glfwTerminate();
}

void Render::init() {
  init_window();
  food_shader_prog_ =
      compile_shader_prog("./assets/food.vert", "./assets/particle.frag");
  scales_shader_prog_ =
      compile_shader_prog("./assets/scales.vert", "./assets/particle.frag");
  init_buffers();
}

void Render::init_window() {
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwSwapInterval(1);

  window_ = glfwCreateWindow(640, 480, "Sneks", NULL, NULL);
  glfwMakeContextCurrent(window_);

  glewExperimental = true;  // Needed in core profile
  if (glewInit() != GLEW_OK) {
    throw std::runtime_error("Failed to initialize GLEW!");
  }

  ImGui_ImplGlfwGL3_Init(window_, true);
}

void Render::init_buffers() {
  // VAO
  glGenVertexArrays(1, &vertex_array_id_);
  glBindVertexArray(vertex_array_id_);

  // VBOs
  static const GLfloat g_vertex_buffer_data[] = {
      -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f,
      -0.5f, 0.5f,  0.0f, 0.5f, 0.5f,  0.0f,
  };
  glGenBuffers(1, &particles_vertex_buffer_);
  glBindBuffer(GL_ARRAY_BUFFER, particles_vertex_buffer_);
  glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data),
               g_vertex_buffer_data, GL_STATIC_DRAW);

  glGenBuffers(1, &food_data_buffer_);
  glBindBuffer(GL_ARRAY_BUFFER, food_data_buffer_);
  glBufferData(GL_ARRAY_BUFFER, 100 * sizeof(Food), NULL, GL_STREAM_DRAW);

  glGenBuffers(1, &scales_data_buffer_);
  glBindBuffer(GL_ARRAY_BUFFER, scales_data_buffer_);
  glBufferData(GL_ARRAY_BUFFER, 1000 * sizeof(Scale), NULL, GL_STREAM_DRAW);
}

void Render::draw() {
  ImGui_ImplGlfwGL3_NewFrame();

  int display_w, display_h;
  glfwGetFramebufferSize(window_, &display_w, &display_h);

  glViewport(0, 0, display_w, display_h);
  glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  switch (Client::instance().stage()) {
    case GameStage::GAME:
      render_field();
      break;
    default:
      break;
  }

  gui_.handle();

  ImGui::Render();
  glfwSwapBuffers(window_);
}

void Render::update_field(std::vector<Food>& foods,
                          std::vector<Scale>& scales) {
  food_buffer_.clear();
  food_buffer_ = std::vector<Food>(foods.begin(), foods.end());
  scales_buffer_.clear();
  scales_buffer_ = std::vector<Scale>(scales.begin(), scales.end());
}

void Render::update_player_head(glm::vec3 pos) { player_head_ = pos; }

void Render::render_field() {
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  static const glm::mat4 proj =
      glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.00001f, 100.0f);
  // static const glm::mat4 proj = glm::ortho(0, 640, 480, 0, 0, 1000);

  glm::vec3 cam = player_head_;
  cam.z = SCALES_PERIOD * 50;
  glm::mat4 view = glm::lookAt(cam,  // Координаты камеры
                               player_head_,  // Направление камеры
                               glm::vec3(0, 1, 0)  // Верх камеры
                               );
  glm::mat4 mvp = proj * view;

  render_food(mvp);
  render_scales(mvp);
}

void Render::render_food(glm::mat4 mvp) {
  // Fill buffers
  glBindBuffer(GL_ARRAY_BUFFER, food_data_buffer_);
  glBufferData(GL_ARRAY_BUFFER, food_buffer_.size() * sizeof(Food), NULL,
               GL_STREAM_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, food_buffer_.size() * sizeof(Food),
                  food_buffer_.data());
  // //
  glUseProgram(food_shader_prog_);

  // Bind uniforms
  GLuint mvp_uniform_id = glGetUniformLocation(food_shader_prog_, "MVP");
  glUniformMatrix4fv(mvp_uniform_id, 1, GL_FALSE, &mvp[0][0]);

  // Bind buffers
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, particles_vertex_buffer_);
  glVertexAttribPointer(0,
                        3,         // size
                        GL_FLOAT,  // type
                        GL_FALSE,  // normalized?
                        0,         // stride
                        (void*)0   // array buffer offset
                        );

  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, food_data_buffer_);
  glVertexAttribPointer(1,
                        3,             // size
                        GL_FLOAT,      // type
                        GL_FALSE,      // normalized?
                        sizeof(Food),  // stride
                        (void*)0       // array buffer offset
                        );

  glVertexAttribDivisor(0, 0);
  glVertexAttribDivisor(1, 1);
  glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, food_buffer_.size());

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
}

void Render::render_scales(glm::mat4 mvp) {
  // Fill buffers
  glBindBuffer(GL_ARRAY_BUFFER, scales_data_buffer_);
  glBufferData(GL_ARRAY_BUFFER, scales_buffer_.size() * sizeof(Scale), NULL,
               GL_STREAM_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, scales_buffer_.size() * sizeof(Scale),
                  scales_buffer_.data());

  // //
  glUseProgram(scales_shader_prog_);

  // Bind uniforms
  GLuint mvp_uniform_id = glGetUniformLocation(scales_shader_prog_, "MVP");
  glUniformMatrix4fv(mvp_uniform_id, 1, GL_FALSE, &mvp[0][0]);

  // Bind buffers
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, particles_vertex_buffer_);
  glVertexAttribPointer(0,
                        3,         // size
                        GL_FLOAT,  // type
                        GL_FALSE,  // normalized?
                        0,         // stride
                        (void*)0   // array buffer offset
                        );

  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glBindBuffer(GL_ARRAY_BUFFER, scales_data_buffer_);
  glVertexAttribPointer(1,
                        3,              // size
                        GL_FLOAT,       // type
                        GL_FALSE,       // normalized?
                        sizeof(Scale),  // stride
                        (void*)0        // array buffer offset
                        );
  glVertexAttribPointer(2,
                        3,                          // size
                        GL_FLOAT,                   // type
                        GL_FALSE,                   // normalized?
                        sizeof(Scale),              // stride
                        (void*)(sizeof(glm::vec3))  // array buffer offset
                        );

  glVertexAttribDivisor(0, 0);
  glVertexAttribDivisor(1, 1);
  glVertexAttribDivisor(2, 1);
  glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, scales_buffer_.size());

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);
}