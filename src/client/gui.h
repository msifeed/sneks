#ifndef GUI_H
#define GUI_H

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

class Gui {
 private:
 public:
  void handle();

 private:
  void menu_screen();
  void lobby_screen();
};

#endif  // GUI_H