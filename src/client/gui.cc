#include "gui.h"

#include "client.h"

void Gui::handle() {
  // {
  //   static float f = 0.0f;
  //   ImGui::Text("Hello, world!");
  //   ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
  //   ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
  //               1000.0f / ImGui::GetIO().Framerate,
  //               ImGui::GetIO().Framerate);
  // }

  switch (Client::instance().stage()) {
    case MAIN_MENU:
      menu_screen();
      break;
    case LOBBY:
      lobby_screen();
      break;
    default:
      break;
  }
}

void Gui::menu_screen() {
  ImGuiWindowFlags window_flags = 0;
  window_flags |= ImGuiWindowFlags_NoTitleBar;
  window_flags |= ImGuiWindowFlags_ShowBorders;
  window_flags |= ImGuiWindowFlags_NoResize;
  window_flags |= ImGuiWindowFlags_NoMove;
  window_flags |= ImGuiWindowFlags_NoScrollbar;
  window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

  ImGui::Begin("Sneks", NULL, window_flags);

  static char address_buf[64] = "localhost";
  static char port_buf[6] = "12345";
  ImGui::InputText("address", address_buf, 64,
                   ImGuiInputTextFlags_CharsNoBlank);
  ImGui::InputText("port", port_buf, 6, ImGuiInputTextFlags_CharsNoBlank |
                                            ImGuiInputTextFlags_CharsDecimal);

  if (ImGui::Button("Connect")) {
    Client::instance().try_connect(address_buf, port_buf);
  }

  ImGui::End();
}

void Gui::lobby_screen() {
  ImGuiWindowFlags window_flags = 0;
  window_flags |= ImGuiWindowFlags_NoTitleBar;
  window_flags |= ImGuiWindowFlags_ShowBorders;
  window_flags |= ImGuiWindowFlags_NoResize;
  window_flags |= ImGuiWindowFlags_NoMove;
  window_flags |= ImGuiWindowFlags_NoScrollbar;
  window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

  ImGui::Begin("", NULL, window_flags);

  static char name_buf[16] = "";
  ImGui::InputText("name", name_buf, 16, ImGuiInputTextFlags_CharsNoBlank);

  if (ImGui::Button("Spawn")) {
    Client::instance().try_spawn(std::string(name_buf));
  }

  ImGui::End();
}