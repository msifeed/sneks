#ifndef RENDER_H
#define RENDER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

#define IMGUI_API
#include "gui.h"
#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

#include <vector>

#include "common/snake.h"

class Render {
 private:
  GLFWwindow* window_;

  std::vector<Food> food_buffer_;
  std::vector<Scale> scales_buffer_;
  glm::vec3 player_head_;

  GLuint vertex_array_id_;
  GLuint particles_vertex_buffer_;
  GLuint food_data_buffer_;
  GLuint scales_data_buffer_;
  GLuint food_shader_prog_;
  GLuint scales_shader_prog_;

  Gui gui_;

 public:
  Render();
  ~Render();

  GLFWwindow* window() { return window_; }
  bool window_should_close() { return glfwWindowShouldClose(window_); }

  void init();
  void shutdown();
  void draw();

  void update_field(std::vector<Food>&, std::vector<Scale>&);
  void update_player_head(glm::vec3);

 private:
  void init_window();
  void init_buffers();

  void render_field();
  void render_food(glm::mat4 mvp);
  void render_scales(glm::mat4 mvp);
};

#endif  // RENDER_H