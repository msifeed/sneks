#ifndef NETWORK_CLIENT_HPP
#define NETWORK_CLIENT_HPP

#include <boost/asio.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <functional>

#include "common/message.h"
#include "common/messagebus.h"
#include "common/session.h"

typedef std::function<void(bool)> ConnectCallback;

class NetworkClient {
 private:
  boost::asio::io_service io_service_;
  boost::asio::deadline_timer deadline_;
  Session session_;
  MessageBus& bus_;

  ConnectCallback connect_cb_;
  SessionCallback write_handler_ =
      std::bind(&NetworkClient::handle_write, this, std::placeholders::_1,
                std::placeholders::_2);

 public:
  NetworkClient(MessageBus& bus, ConnectCallback cb)
      : deadline_(io_service_),
        session_(io_service_),
        bus_(bus),
        connect_cb_(cb) {}

  void connect(const std::string&, const std::string&);
  void stop();
  void send(std::unique_ptr<Message> msg);

 private:
  void check_deadline();
  void handle_connect(const boost::system::error_code&);
  void handle_read(const boost::system::error_code&, Session&);
  void handle_write(const boost::system::error_code&, Session&);
};

#endif  // NETWORK_CLIENT_HPP