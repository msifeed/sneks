#include "server.h"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Message)
BOOST_CLASS_EXPORT(MessageSpawn)
BOOST_CLASS_EXPORT(MessageTurn)
BOOST_CLASS_EXPORT(MessageObjects)

int main(int, char**) {
  Server server;
  server.run();

  return 0;
}
