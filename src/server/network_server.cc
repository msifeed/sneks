#include "network_server.h"

void NetworkServer::run() {
  // Start accept connections
  accept_next();

  io_service_.run();
}

void NetworkServer::stop() { io_service_.stop(); }

void NetworkServer::write_to(uint32_t sess_id, std::unique_ptr<Message> msg) {
  std::lock_guard<std::mutex> lck(mutex_);
  Session_ptr sess = sessions_.at(sess_id);
  sess->queue_message(std::move(msg));
  sess->async_write(write_handler_);
}

std::list<std::pair<uint32_t, bool>> NetworkServer::session_updates() {
  std::lock_guard<std::mutex> lck(mutex_);
  std::list<std::pair<uint32_t, bool>> tmp;
  tmp.swap(session_updates_);
  return tmp;
}

void NetworkServer::accept_next() {
  Session_ptr new_session(new Session(io_service_));

  acceptor_.async_accept(new_session->socket(),
                         std::bind(&NetworkServer::handle_accept, this,
                                   std::placeholders::_1, new_session));
}

void NetworkServer::handle_accept(const boost::system::error_code& ec,
                                  Session_ptr sess) {
  if (ec) {
    std::cerr << "Server connect error:" << ec.message() << std::endl;
  } else {
    handle_session(sess);
  }

  accept_next();
}

void NetworkServer::handle_session(Session_ptr sess) {
  sess->async_read(std::bind(&NetworkServer::handle_read, this,
                             std::placeholders::_1, std::placeholders::_2));
  uint32_t id = 0;

  // Find first free place
  for (std::size_t i = 0; i < sessions_.size(); ++i) {
    if (sessions_[i].get() == nullptr) {
      id = i;
      break;
    }
  }
  if (id == 0) {
    id = sessions_.size();
    sessions_.resize(id + 1);
  }

  sess->set_id(id);
  sessions_.at(id) = std::move(sess);

  std::lock_guard<std::mutex> lck(mutex_);
  session_updates_.push_back(std::pair<uint32_t, bool>(id, true));
}

void NetworkServer::handle_disconnect(Session& sess) {
  std::cout << "Session removed: " << sess.id() << std::endl;
  sessions_.at(sess.id()).reset();

  std::lock_guard<std::mutex> lck(mutex_);
  session_updates_.push_back(std::pair<uint32_t, bool>(sess.id(), false));
}

void NetworkServer::handle_read(const boost::system::error_code& ec,
                                Session& sess) {
  if (ec) {
    if (ec == boost::asio::error::eof ||
        ec == boost::asio::error::connection_reset) {
      handle_disconnect(sess);
    } else {
      std::cerr << "Server read error: " << ec.message() << std::endl;
    }
  } else {
    bus_.put_all(sess.read_queue());
  }
}

void NetworkServer::handle_write(const boost::system::error_code& ec,
                                 Session&) {
  if (ec) {
    std::cerr << "Server write error: " << ec.message() << std::endl;
  }
}
