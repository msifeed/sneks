#ifndef NETWORK_SERVER_H
#define NETWORK_SERVER_H

#include <boost/asio.hpp>
#include <functional>
#include <list>
#include <mutex>
#include <vector>

#include "common/messagebus.h"
#include "common/session.h"

using boost::asio::ip::tcp;

class NetworkServer {
 private:
  std::mutex mutex_;

  boost::asio::io_service io_service_;
  tcp::acceptor acceptor_;

  MessageBus& bus_;
  std::vector<Session_ptr> sessions_;
  std::list<std::pair<uint32_t, bool>> session_updates_;

  SessionCallback write_handler_ =
      std::bind(&NetworkServer::handle_write, this, std::placeholders::_1,
                std::placeholders::_2);

 public:
  NetworkServer(short port, MessageBus& bus)
      : acceptor_(io_service_, tcp::endpoint(tcp::v4(), port)), bus_(bus) {}

  void run();
  void stop();
  void write_to(uint32_t sess_id, std::unique_ptr<Message> msg);
  std::list<std::pair<uint32_t, bool>> session_updates();

 private:
  void accept_next();
  void handle_accept(const boost::system::error_code&, Session_ptr);
  void handle_session(Session_ptr);
  void handle_disconnect(Session&);
  void handle_read(const boost::system::error_code&, Session&);
  void handle_write(const boost::system::error_code&, Session&);
};

#endif  // NETWORK_SERVER_H