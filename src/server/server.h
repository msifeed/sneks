#ifndef SERVER_H
#define SERVER_H

#include <algorithm>
#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <random>
#include <thread>

#include "common/messagebus.h"
#include "network_server.h"

class Player {
 private:
  uint32_t session_id_ = 0;
  Snake_ptr snake_;

 public:
  Player(uint32_t session_id) : session_id_(session_id) {}

  uint32_t sess_id() { return session_id_; }
  Snake_ptr& snake() { return snake_; }
  void set_snake(Snake_ptr s) { snake_ = s; }
  bool alive() { return snake_ == nullptr; }

  bool operator<(const Player& other) const {
    return session_id_ < other.session_id_;
  }
};

typedef std::shared_ptr<Player> Player_ptr;

class Server {
 private:
  MessageBus bus_;
  NetworkServer network_;
  std::thread network_thread_;

  std::vector<Player_ptr> players_;
  std::vector<Snake_ptr> snakes_;
  std::list<Food> foods_;

  uint64_t tick = 0;
  uint32_t radius = 100;
  bool is_running_ = true;

 public:
  Server() : network_(12345, bus_) {}

  void run();
  void stop();

 private:
  void check_sessions();
  void check_messages();
  std::vector<Food> get_visible_food_at(glm::vec2 pos, uint32_t len);
  std::vector<Scale> get_visible_scales_at(glm::vec2);

  void generate_food();
  void kill_snake(Player_ptr& pl);

  void handle_player(Player_ptr& pl);
  void handle_join(uint32_t sid);
  void handle_disconnect(uint32_t sid);
  void handle_spawn(MessageSpawn& msg);
  void handle_turn(MessageTurn& msg);
};

#endif  // SERVER_H