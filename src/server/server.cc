#include "server.h"

void Server::run() {
  network_thread_ = std::thread(&NetworkServer::run, &network_);

  std::cout << "Server started" << std::endl;
  while (is_running_) {
    generate_food();
    check_sessions();
    check_messages();

    for (Player_ptr& pl : players_) {
      if (pl) handle_player(pl);
    }

    tick += 1;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }

  std::cout << "Server stop" << std::endl;
  network_.stop();
  network_thread_.join();
}

void Server::stop() { is_running_ = false; }

void Server::check_sessions() {
  for (auto& pair : network_.session_updates()) {
    if (std::get<1>(pair))
      handle_join(std::get<0>(pair));
    else
      handle_disconnect(std::get<0>(pair));
  }
}

void Server::check_messages() {
  for (auto& msg : bus_.consume_messages()) {
    MessageType type = msg->type();
    switch (type) {
      case MessageType::SPAWN: {
        handle_spawn(static_cast<MessageSpawn&>(*msg));
      } break;
      case MessageType::TURN: {
        handle_turn(static_cast<MessageTurn&>(*msg));
      } break;
      default:
        break;
    }
  }
}

void Server::handle_player(Player_ptr& pl) {
  auto snake = pl->snake();
  if (!snake) {
    return;
  }

  static const float max_turn = 0.3f;
  static const float pi = glm::pi<float>();

  float diff = snake->next_direction_ - snake->direction_;
  if (glm::abs(diff) > pi &&
      glm::sign(snake->next_direction_) != glm::sign(snake->direction_)) {
    float mod = 2 * pi * glm::sign(diff);
    snake->direction_ += glm::clamp(diff - mod, -max_turn, max_turn) + mod;
  } else {
    snake->direction_ += glm::clamp(diff, -max_turn, max_turn);
  }

  auto last_pos = snake->head().pos;
  for (Scale& sc : snake->scales_) {
    std::swap(sc.pos, last_pos);
  }

  auto& head = snake->scales_.front();
  head.pos.x += glm::sin(snake->direction_) * SCALES_PERIOD;
  head.pos.y -= glm::cos(snake->direction_) * SCALES_PERIOD;

  for (Snake_ptr sn : snakes_) {
    if (sn == snake) {
      continue;
    }

    if (glm::distance((glm::vec2)head.pos, (glm::vec2)sn->head().pos) >
        sn->len()) {
      continue;
    }

    for (Scale sc : sn->scales_) {
      if (glm::distance((glm::vec2)head.pos, (glm::vec2)sc.pos) < 1.0) {
        kill_snake(pl);
        auto msg = new MessageSpawn();
        msg->name = "die";
        network_.write_to(pl->sess_id(), std::unique_ptr<Message>(msg));
        return;
      }
    }
  }

  std::list<Food>::iterator iter = foods_.begin();
  while (iter != foods_.end()) {
    if (glm::distance((glm::vec2)head.pos, (glm::vec2)(*iter).pos) < 1.0) {
      foods_.erase(iter++);

      Scale sc;
      sc.pos = foods_.back().pos;
      sc.color = snake->color_;
      if (snake->scales_.size() % 2 == 0) sc.color *= 0.8;

      snake->scales_.push_back(sc);
    } else {
      iter++;
    }
  }

  auto msg = new MessageObjects();
  msg->foods = get_visible_food_at(head.pos, snake->len());
  msg->scales = get_visible_scales_at(head.pos);
  msg->player_head = head.pos;

  network_.write_to(pl->sess_id(), std::unique_ptr<Message>(msg));
}

std::vector<Food> Server::get_visible_food_at(glm::vec2 pos, uint32_t len) {
  std::vector<Food> ret;

  for (Food f : foods_) {
    if (glm::distance(pos, (glm::vec2)f.pos) < len) {
      ret.push_back(f);
    }
  }

  return ret;
}

std::vector<Scale> Server::get_visible_scales_at(glm::vec2 pos) {
  std::vector<Scale> ret;

  for (Snake_ptr s : snakes_) {
    auto head = s->head();
    auto len = s->len();
    // len *= len / 10; // scale scales

    // filter distant snakes
    if (glm::distance(pos, (glm::vec2)head.pos) > len) {
      continue;
    }

    ret.insert(ret.end(), s->scales_.begin(), s->scales_.end());
  }

  return ret;
}

void Server::generate_food() {
  static std::default_random_engine gen;
  static std::normal_distribution<float> dist(0.0, 1.0);

  static uint64_t total_food_target = 1000;
  long foods_to_add = total_food_target - foods_.size();

  if (foods_to_add < 1) return;

  std::vector<Food> new_food;
  new_food.reserve(foods_to_add);

  for (int i = 0; i < foods_to_add; ++i) {
    float x = this->radius * dist(gen);
    float y = this->radius * dist(gen);
    Food f;
    f.pos = glm::vec3(x, y, 1.0);

    new_food.push_back(f);
  }

  foods_.insert(foods_.end(), new_food.begin(), new_food.end());
}

void Server::kill_snake(Player_ptr& pl) {
  snakes_.erase(std::remove(snakes_.begin(), snakes_.end(), pl->snake()),
                snakes_.end());
  pl->set_snake(nullptr);
}

void Server::handle_join(uint32_t sid) {
  std::cout << "Someone joined" << std::endl;

  auto new_player = new Player(sid);

  if (players_.size() <= sid) {
    players_.resize(sid + 1);
  }
  players_.at(sid) = Player_ptr(new_player);
}

void Server::handle_spawn(MessageSpawn& msg) {
  static std::random_device rd;
  static std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(0.1, 0.9);

  auto& pl = players_.at(msg.sid);
  if (pl->snake()) {
    std::cerr << "Player already has snake!" << std::endl;
    return;
  }

  Snake* s = new Snake(msg.name);
  s->direction_ = 0;
  s->color_ = glm::vec3(dis(gen) * 0.8, dis(gen), dis(gen) * 0.5);

  auto last_scale_pos = glm::vec3(0, 0, 0.01 * msg.sid);
  for (int i = 0; i < 20; ++i) {
    last_scale_pos.x += SCALES_PERIOD;
    last_scale_pos.z -= 0.00001;

    Scale sc;
    sc.pos = last_scale_pos;
    sc.color = s->color_;
    if (i % 2 == 1) sc.color *= 0.8;  // чередование темных чешуек

    s->scales_.push_back(sc);
  }

  auto snake_ptr = Snake_ptr(s);
  pl->set_snake(snake_ptr);
  snakes_.push_back(std::move(snake_ptr));

  std::cout << "Player: " << msg.sid << " Snake spawned: " << pl->snake()->name_
            << std::endl;

  auto spawn_msg = new MessageSpawn();
  spawn_msg->name = "spawn";
  network_.write_to(pl->sess_id(), std::unique_ptr<Message>(spawn_msg));
}

void Server::handle_turn(MessageTurn& msg) {
  // std::cout << "MessageTurn: angle: " << msg.angle << std::endl;
  auto& pl = players_.at(msg.sid);
  auto& snake = pl->snake();
  if (snake) {
    snake->next_direction_ = msg.angle;
  }
}

void Server::handle_disconnect(uint32_t sid) {
  std::cout << "Player disconnected: " << sid << std::endl;

  Player_ptr& pl = players_.at(sid);

  kill_snake(pl);

  pl.reset();
  bus_.remove_with_sid(sid);
}