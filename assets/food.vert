#version 330 core

layout(location = 0) in vec3 vertices;
layout(location = 1) in vec3 pos;

layout(location = 0) out vec3 out_color;

uniform mat4 MVP;

void main() {
	vec3 vertex_pos = pos + vertices;

	gl_Position = MVP * vec4(vertex_pos, 1.0);
	out_color = fract(pos);
}