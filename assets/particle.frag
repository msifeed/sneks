#version 330 core

layout(location = 0) in vec3 color;
out vec3 out_color;

void main() {
	out_color = color;
}